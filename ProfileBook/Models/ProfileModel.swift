public struct ProfileModel
{
    var Id : Int64
    var Name = ""
    var Description = ""
    var ImagePath = ""
    var UserId : Int64
}
