import Foundation
import SQLite

public class RepositoryService
{
    private static var _instance : RepositoryService! = nil
    public static var Instance : RepositoryService
    {
        get{
            
            if _instance == nil {
                _instance = RepositoryService()
                _instance.Initialize()
            }
            
            return _instance
        }
    }
    
    let dbName = "db.sqlite"
    
    var db : Connection!
    
    let usersTable = Table("users")
    let profileTable = Table("profile")
    
    let idField = Expression<Int64>("id")
    
    let loginField = Expression<String>("name")
    let passwordField = Expression<String>("password")
    
    let userIdField = Expression<Int64>("userId")
    let nameField = Expression<String>("name")
    let descriptionField =  Expression<String>("description")
    let imagePathField =  Expression<String>("image_path")
    
    public func Initialize()
    {
        openDatabaseConnection()
        
        EnsureTableCreated()
    }
    
    public func GetUser(login : String?, password : String?) -> UserModel!
    {
        do {
            let res = try db.prepare(usersTable).first{c in try c.get(loginField) == login && c.get(passwordField) == password}
            if(res != nil)
            {
                return UserModel(Id: try res!.get(idField), Login:try res!.get(loginField), Password:try res!.get(passwordField))
            }
            else
            {
                return nil
            }
            } catch  {
            return nil
        }
    }
    
    public func AddUser(user : UserModel) -> Int64
    {
        do
        {
            return try db.run(usersTable.insert(loginField <- user.Login, passwordField <- user.Password))
        }
        catch
        {
            return -1
        }
    }
    
    public func GetProfile(id : Int64) -> ProfileModel?
    {
        var profile : ProfileModel? = nil
        
        do{
            let res = try db.prepare(profileTable).first {p in try p.get(idField) == id}
            
            if res != nil{
                profile = ProfileModel(Id: try res!.get(idField), Name:try res!.get(nameField), Description:try res!.get(descriptionField), ImagePath: try res!.get(imagePathField) ,UserId : try res!.get(userIdField))
            }
        }
        catch
        {
        }
        
        return profile;
    }
    
    public func GetAllProfiles(userId : Int64) -> [ProfileModel]
    {
        var result = [ProfileModel]()
            
        do
        {
            let usersProfiles = profileTable.filter(userIdField == userId)
                
            for profile in try db.prepare(usersProfiles)
            {
                result.append(ProfileModel(Id: try profile.get(idField), Name: try profile.get(nameField), Description: try profile.get(descriptionField),ImagePath: try profile.get(imagePathField), UserId: try profile.get(userIdField)))
            }
                
        }
        catch
        {
        }
        
        return result
    }
    
    public func AddProfile(profile : ProfileModel) -> Int64
    {
        do
        {
            return try db.run(profileTable.insert(nameField <- profile.Name, descriptionField <- profile.Description,imagePathField <- profile.ImagePath, userIdField <- profile.UserId))
        }
        catch
        {
            return -1
        }
    }
    
    public func UpdateProfile(profile : ProfileModel)
    {
        do
        {
            let profileRow = profileTable.filter(idField == profile.Id)
            try db.run(profileRow.update(nameField <- profile.Name, descriptionField <- profile.Description,imagePathField <- profile.ImagePath, userIdField <- profile.UserId))
        }
        catch
        {
        }
    }
    
    public func DeleteProfile(profile : ProfileModel)
    {
        do
        {
            let profileRow = profileTable.filter(idField == profile.Id)
            try db.run(profileRow.delete())
        }
        catch
        {
        }
    }
    
    private func ClearDB()
    {
        let fileManager = FileManager.default
          let folderPath = getDocumentsDirectory()
          do {
            try fileManager.removeItem(atPath: "\(folderPath)/\(dbName)" )
          } catch {
            print("Could not clear temp folder: \(error)")
          }
    }
    
    private func EnsureTableCreated()
    {
        do
        {
            try db.run(usersTable.create { t in
                t.column(idField, primaryKey: .autoincrement)
                t.column(loginField, unique: true)
                t.column(passwordField)
            })
            
            try db.run(profileTable.create { t in
                t.column(idField, primaryKey: .autoincrement)
                t.column(nameField, unique: true)
                t.column(descriptionField)
                t.column(imagePathField)
                t.column(userIdField)
            })
        }
        catch{
            
        }
    }
    
    private func openDatabaseConnection()
    {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
        ).first!
        
        do
        {
            db = try Connection("\(path)/\(dbName)")
        }
        catch{
        }
    }
    
    private func getDocumentsDirectory() -> String {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory.absoluteString
    }
}
