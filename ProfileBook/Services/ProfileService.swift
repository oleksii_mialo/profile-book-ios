public class ProfileService
{
    private static var _instance : ProfileService! = nil
    public static var Instance : ProfileService
    {
        get{
            
            if _instance == nil {
                _instance = ProfileService()
            }
            
            return _instance
        }
    }
    
    public func AddOrUpdateProfile(_ profileModel : ProfileModel) -> Bool
    {
        let profile = RepositoryService.Instance.GetProfile(id: profileModel.Id)
        
        if profile != nil
        {
            RepositoryService.Instance.UpdateProfile(profile: profileModel)
        }
        else
        {
            RepositoryService.Instance.AddProfile(profile: profileModel)
        }
        
        return true
    }
    
    public func DeleteProfile(_ profileModel : ProfileModel) -> Bool
    {
        RepositoryService.Instance.DeleteProfile(profile: profileModel)
        
        return true
    }
    
    public func GetAllProfiles() -> [ProfileModel]
    {
        if AuthenticationService.Instance.IsUserAuthenticated
        {
            return RepositoryService.Instance.GetAllProfiles(userId: AuthenticationService.Instance.CurrentUserId!)
        }
        else
        {
            return [ProfileModel]()
        }
        
    }
}

import Foundation
import UIKit

class ImageFileHelper
{
  func save(image: UIImage) -> String? {
      let fileName = UUID().uuidString + ".jpeg"
      let fileURL = documentsUrl.appendingPathComponent(fileName)
      if let imageData = image.jpegData(compressionQuality: 1.0) {
          try? imageData.write(to: fileURL, options: .atomic)
          return fileName
      }
      
    print("Error saving image")
    return nil
  }
   
  func load(fileName: String) -> UIImage? {
      let fileURL = documentsUrl.appendingPathComponent(fileName)
      do {
          let imageData = try Data(contentsOf: fileURL)
          return UIImage(data: imageData)
      } catch {
          print("Error loading image : \(error)")
      }
    return nil
  }
   
  private var documentsUrl: URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
  }
}
