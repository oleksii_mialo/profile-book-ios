import Foundation
import UIKit

public class ImageSaverService {
    private let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    private static var _instance : ImageSaverService! = nil
    public static var Instance : ImageSaverService
    {
        get{
            
            if _instance == nil {
                _instance = ImageSaverService()
            }
            
            return _instance
        }
    }
    
    public func saveImage(image: UIImage) -> String {
        var result = ""
        
        do {
            let fileName = UUID().uuidString
            let filePath = documentsUrl.appendingPathComponent(fileName)
            if let imageData = image.jpegData(compressionQuality: 1.0) {
                try imageData.write(to: filePath, options: .atomic)
                result = fileName
            }
        }
        catch {
        }
        
        return result
    }
    
    public func loadImage(fileName: String) -> UIImage? {
        var result: UIImage? = nil
        
        do {
            let filePath = documentsUrl.appendingPathComponent(fileName)
            let imageData = try Data(contentsOf: filePath)
            result = UIImage(data: imageData)
        }
        catch {
        }
        
        return result
    }

}
