import Foundation
import SQLite

public class AuthenticationService
{
    private static var _instance : AuthenticationService! = nil
    public static var Instance : AuthenticationService
    {
        get{
            
            if _instance == nil {
                _instance = AuthenticationService()
            }
            
            return _instance
        }
    }
    
    public var CurrentUserId : Int64?
    
    public var IsUserAuthenticated : Bool{
        get {
            return self.CurrentUserId != nil
        }
    }
    
    public func SignUp(_ login : String, _ password : String)
    {
        RepositoryService.Instance.AddUser(user: UserModel(Id: 0, Login: login, Password: password));
    }
    
    public func SignIn(_ login : String?, _ password : String?) -> Bool
    {
        let user = RepositoryService.Instance.GetUser(login: login, password: password)
        
        if user != nil
        {
            CurrentUserId = user?.Id
        }
        
        return user != nil
    }
}
