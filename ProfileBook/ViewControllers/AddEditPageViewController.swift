import Foundation
import UIKit

class AddEditProfilePageViewController : UIViewController, NavigationProtocol, UIImagePickerControllerDelegate & UINavigationControllerDelegate
{
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var descritpionField: UITextField!
    
    var isEdit : Bool = false
    var Profile : ProfileModel! = nil
    
    var PreviousViewController: NavigationProtocol?
    
    var navigationParameters : [String : Any?]!
    
    func OnNavigatedTo(parameters: [String : Any?]) {
        navigationParameters = parameters
    }

    var imagePath : String! = "default"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
        if navigationParameters != nil
        {
            if navigationParameters.contains(where: {(key, value) in key == "PROFILE"}){
                isEdit = true
                Profile = navigationParameters["PROFILE"] as? ProfileModel
                nameField.text = Profile.Name
                descritpionField.text = Profile.Description
                
                do{
                    let helper = ImageFileHelper()
                    
                    imageButton.setImage(helper.load(fileName: Profile.ImagePath), for: .normal)
                }
                catch
                {
                }
            }
        }
    }
    
    @IBOutlet weak var imageButton: UIButton!
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        do{
            let imageHelper = ImageFileHelper()
            
            let image = info[.originalImage] as? UIImage

            let path = imageHelper.save(image: image!)
            
            self.imagePath = path
                
            imageButton.setBackgroundImage(image, for: .normal)
        }
        catch
        {
        }
    }
    @IBAction func FirstTapped(_ sender: Any) {
        
        let ui = UIImagePickerController()
//
        ui.delegate = self
//
//        self.present(ui, animated: true)
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    
    let cameraAction = UIAlertAction(title: "Camera", style: .default)
    {
        UIAlertAction in
        alert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            ui.sourceType = .camera
            self.present(ui, animated: true, completion: nil)
        }
    }
    let galleryAction = UIAlertAction(title: "Gallery", style: .default){
        UIAlertAction in
        alert.dismiss(animated: true, completion: nil)
        ui.sourceType = .photoLibrary
        self.present(ui, animated: true, completion: nil)
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
        UIAlertAction in
    }
    
    alert.addAction(cameraAction)
    alert.addAction(galleryAction)
    alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.sourceView = self.view

        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func OnAddProfileTapped(_ sender: Any) {
        
        if(nameField.hasText && descritpionField.hasText)
        {
            ProfileService.Instance.AddOrUpdateProfile(ProfileModel(Id: Profile?.Id ?? -1, Name: nameField.text!, Description: descritpionField.text!,ImagePath: imagePath, UserId: AuthenticationService.Instance.CurrentUserId!))
            PreviousViewController?.OnNavigatedTo(parameters: [:])
        navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
