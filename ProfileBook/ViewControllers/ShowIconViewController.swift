import Foundation
import UIKit


class ShowIconViewController : UIViewController, NavigationProtocol
{
    var PreviousViewController: NavigationProtocol?
    
    private var imageSource : UIImage?
    func OnNavigatedTo(parameters: [String : Any?]) {
        
        if parameters.contains(where: {(key, value) in key == "IMAGE"}){
            imageSource = parameters["IMAGE"] as? UIImage
        }
    }
    
    @IBOutlet weak var imageField: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        imageField.image = imageSource
    }
    
    @objc func dismissKeyboard(obj : Any) {
        navigationController!.popViewController(animated: true)
    }
}
