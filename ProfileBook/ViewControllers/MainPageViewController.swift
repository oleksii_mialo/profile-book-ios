import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var descriptionField: UILabel!
}

class MainPageViewController:  UIViewController, UITableViewDelegate, UITableViewDataSource, NavigationProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    
    var PreviousViewController: NavigationProtocol?
    
    func OnNavigatedTo(parameters: [String : Any?]) {
        reloadData()
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        return UIContextMenuConfiguration(identifier: nil,
                                              previewProvider: nil,
                                              actionProvider: {
                    suggestedActions in
                let editAction =
                    UIAction(title: NSLocalizedString("EditAction", comment: ""),
                             image: UIImage(systemName: "pencil.circle")) { action in
                        self.performEdit(indexPath)
                    }
                let deleteAction =
                    UIAction(title: NSLocalizedString("DeleteAction", comment: ""),
                             image: UIImage(systemName: "trash.circle")) { action in
                        self.performDelete(indexPath)
                    }
                return UIMenu(title: "", children: [editAction, deleteAction])
            })
    }
    
    func reloadData()
    {
        self.data = ProfileService.Instance.GetAllProfiles();
        tableView.reloadData()
    }
    
    func performEdit(_ indexPath: IndexPath)
    {
        self.performSegue(withIdentifier: "AddEditProfileSegue", sender: indexPath)
    }
    
    func performDelete(_ indexPath: IndexPath)
    {
        let profileModel = data[indexPath.row]
        
        ProfileService.Instance.DeleteProfile(profileModel)
        
        reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell else {
                   fatalError("Unable to dequeue ReminderCell")
               }
        
        cell.nameField?.text = data[indexPath.row].Name
        cell.descriptionField?.text = data[indexPath.row].Description
        
        do
        {
            let imagePath = data[indexPath.row].ImagePath;
            
            if(imagePath == "default")
            {
                cell.imageField.image = UIImage(systemName: "arrow.up.square")
            }
            else
            {
            var helper = ImageFileHelper()
            
            cell.imageField.image = helper.load(fileName: imagePath)
            }
            
        }
        catch
        {
            print(error)
        }
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(performShowIcon))
        
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    @objc func performShowIcon(recognizer : UITapGestureRecognizer)
    {
        let cell = (recognizer.view as? ProfileTableViewCell)
        
        self.performSegue(withIdentifier: "ShowIconSegue", sender: cell?.imageField.image)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    
    var data : [ProfileModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.data = ProfileService.Instance.GetAllProfiles();
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var navigationProtocol = (segue.destination as? NavigationProtocol)
        navigationProtocol?.PreviousViewController = self
        
        let path = sender as? IndexPath;
        
        if(path != nil)
        {
            let index = path!.row
            
            navigationProtocol?.OnNavigatedTo(parameters: ["PROFILE": data[index]])
        }
        else
        {
            let image = sender as? UIImage
            
            navigationProtocol?.OnNavigatedTo(parameters: ["IMAGE":image])
        }
    }
}
