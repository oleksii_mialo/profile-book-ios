import UIKit

class SignUpViewController : UIViewController, NavigationProtocol
{
    @IBOutlet weak var _loginEntry: UITextField!
    @IBOutlet weak var _passwordTextField: UITextField!
    @IBOutlet weak var _confirmPasswordTextField: UITextField!
    
    var PreviousViewController: NavigationProtocol?
    
    func OnNavigatedTo(parameters: [String : Any?]) {
    }

    @IBAction func OnSignUpTapped(_ sender: Any) {
        if _passwordTextField.text == _confirmPasswordTextField.text
        {
            if Validator.IsLoginValid(login: _loginEntry.text) && Validator.IsPasswordValid(password: _passwordTextField.text)
            {
                AuthenticationService.Instance.SignUp(_loginEntry.text ?? "", _passwordTextField.text ?? "")
                
                self.PreviousViewController?.OnNavigatedTo(parameters: ["LOGIN":_loginEntry.text])
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
