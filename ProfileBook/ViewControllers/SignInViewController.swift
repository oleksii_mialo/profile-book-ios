import UIKit



class SignInViewController: UIViewController, NavigationProtocol {
    var PreviousViewController: NavigationProtocol?
    
    func OnNavigatedTo(parameters: [String : Any?]) {
        
        if parameters.contains(where: {(key, value) in key == "LOGIN"}){
            loginField.text = parameters["LOGIN"] as? String
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var navigationProtocol = (segue.destination as? NavigationProtocol)
        navigationProtocol?.PreviousViewController = self
        
    }
    
    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
    }

    @IBAction func OnSignInTapped(_ sender: Any) {
        if AuthenticationService.Instance.SignIn(loginField.text, passwordField.text)
        {
            self.performSegue(withIdentifier: "OnSignInTappedSegue", sender: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Hnuk hnuk", message: "Error", preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                        //Cancel Action
           }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

