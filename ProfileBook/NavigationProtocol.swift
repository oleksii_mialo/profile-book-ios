//
//  ViewControllerProtocol.swift
//  ProfileBook
//
//  Created by User on 18.10.2021.
//

import Foundation
import UIKit

protocol NavigationProtocol {
    
    var  PreviousViewController : NavigationProtocol? {get set}
    
    func OnNavigatedTo(parameters: [String: Any?])
}
